//! This module contains the game aspects.
extern crate rand;

use figure::{Apple, Snake};
use point::Direction::*;
use point::{Direction, Point};
use std::fmt;
use std::default::Default;
use std::io;
use std::io::{Stdout, Write};
use termion::{clear, color, cursor, style};
use termion::event::Key;
use termion::raw::RawTerminal;
use traits::Position;

/// Represents the state of the game.
pub struct Game {
    field_width: u16,
    field_height: u16,
    apple: Apple,
    snake: Snake,
    direction: Direction,
    stdout: RawTerminal<Stdout>,
    game_over: bool,
    score: u16,
}

impl Game {
    pub fn new(field_width: u16, field_height: u16, stdout: RawTerminal<Stdout>) -> Self {
        Game {
            field_width,
            field_height,
            apple: Apple::new(Point::new(field_width - 1, field_height - 1)),
            snake: Snake::default(),
            direction: Right,
            stdout,
            game_over: false,
            score: 0,
        }
    }

    pub fn input(&mut self, key: Option<Key>) {
        if let Some(key) = key {
            if let Some(direction) = key_to_direction(key) {
                // Only update direction if it's orthogonal
                // Same direction again has no effect
                // Opposite direction would stop the snake
                // or game over
                //
                // old        | new        | change
                // --------------------------------
                // Up,Down    | Up,Down    | false
                // Up,Down    | Left,Right | true
                // Left,Right | Up,Down    | true
                // Left,Right | Left,Right | false
                if direction != self.direction {
                    if (self.direction == Up && direction != Down)
                        || (self.direction == Down && direction != Up)
                        || (self.direction == Left && direction != Right)
                        || (self.direction == Right && direction != Left)
                    {
                        self.direction = direction;
                    }
                }
            }
        }

        if self.collision_next_step() {
            self.game_over().expect("Display Game Over");
            self.game_over = true;
            return;
        } else if self.apple_in_front() {
            self.snake.grow(&self.direction);
            self.new_apple();
            self.score += 1;
        } else {
            self.snake.crawl(&self.direction);
        }
    }

    /// Retunrs `true` if game is over because of collision.
    pub fn over(&self) -> bool {
        self.game_over
    }

    /// Clear display and reset cursor before quitting game.
    pub fn quit(&mut self) -> io::Result<()> {
        write!(&mut self.stdout, "{}{}", clear::All, cursor::Goto(1, 1))
    }

    /// Display the current state on the screen.
    pub fn display(&mut self) -> io::Result<()> {
        if self.game_over {
            self.game_over()?;
        } else {
            self.draw_all()?;
        }
        Ok(())
    }

    /// Draw the field on the screen.
    fn draw_all(&mut self) -> io::Result<()> {
        self.clear_display()?;
        self.set_bg_color()?;
        self.draw_top_border()?;

        for line in 0..self.field_height {
            // Left border
            write!(&mut self.stdout, "║")?;
            for row in 0..self.field_width {
                let point = Point::new(row, line);
                if point.collision(&self.apple) {
                    write!(&mut self.stdout, "{}", self.apple)?;
                } else if point.collision(&self.snake) {
                    if point == self.snake.head() {
                        write!(
                            &mut self.stdout,
                            "{yellow}ö{reset}",
                            yellow = color::Fg(color::Yellow),
                            reset = color::Fg(color::Reset)
                        )?;
                    } else {
                        write!(
                            &mut self.stdout,
                            "{green}o{reset}",
                            green = color::Fg(color::Green),
                            reset = color::Fg(color::Reset)
                        )?;
                    }
                } else {
                    write!(&mut self.stdout, " ")?;
                }
            }
            // Right border
            write!(&mut self.stdout, "║\n\r")?;
        }

        self.draw_between_border()?;
        self.draw_score_time()?;
        self.draw_bottom_border()?;

        self.draw_reset()?;

        self.stdout.flush().expect("Flushing stdout");
        Ok(())
    }

    fn clear_display(&mut self) -> io::Result<()> {
        write!(
            &mut self.stdout,
            "{}{}",
            cursor::Goto(1, 1),
            clear::AfterCursor
        )
    }

    fn set_bg_color(&mut self) -> io::Result<()> {
        // Brownish
        write!(&mut self.stdout, "{}", color::Bg(color::Rgb(110, 0, 0)))
    }

    fn draw_top_border(&mut self) -> io::Result<()> {
        write!(&mut self.stdout, "╔")?;
        for _ in 0..self.field_width {
            write!(&mut self.stdout, "═")?;
        }
        write!(&mut self.stdout, "╗\n\r")
    }

    fn draw_between_border(&mut self) -> io::Result<()> {
        write!(&mut self.stdout, "╠")?;
        for _ in 0..self.field_width {
            write!(&mut self.stdout, "═")?;
        }
        write!(&mut self.stdout, "╣\n\r")
    }

    fn draw_score_time(&mut self) -> io::Result<()> {
        let txt = "Score ";
        let score = format!("{}", self.score);
        write!(&mut self.stdout, "║")?;
        write!(&mut self.stdout, "{} ", txt)?;
        write!(&mut self.stdout, "{}", style::Bold)?;
        write!(&mut self.stdout, "{}", score)?;
        for _ in 0..(self.field_width as usize - txt.len() - score.len() - 1) {
            write!(&mut self.stdout, " ")?;
        }
        write!(&mut self.stdout, "║\n\r")
    }

    fn draw_bottom_border(&mut self) -> io::Result<()> {
        write!(&mut self.stdout, "╚")?;
        for _ in 0..self.field_width {
            write!(&mut self.stdout, "═")?;
        }
        write!(&mut self.stdout, "╝\n\r")
    }

    fn draw_reset(&mut self) -> io::Result<()> {
        write!(&mut self.stdout, "{}", color::Bg(color::Reset))?;
        write!(&mut self.stdout, "{}", color::Fg(color::Reset))
    }

    pub fn restart(&mut self) {
        self.snake = Snake::default();
        self.apple = Apple::new(Point::new(self.field_width - 1, self.field_height - 1));
        self.direction = Right;
        self.game_over = false;
    }

    /// Displaying Game Over in the middle of the field.
    fn game_over(&mut self) -> io::Result<()> {
        let txt = "GAME OVER";
        let mid_x = (self.field_width - (txt.len() as u16)) / 2 + 2;
        let mid_y = self.field_height / 2;
        write!(&mut self.stdout, "{}", cursor::Goto(mid_x, mid_y - 1))?;

        self.set_bg_color()?;

        // Top border
        write!(&mut self.stdout, "╭")?;
        for _ in 0..txt.len() {
            write!(&mut self.stdout, "╌")?;
        }
        write!(&mut self.stdout, "╮")?;

        // Left border
        write!(&mut self.stdout, "{}", cursor::Goto(mid_x, mid_y))?;
        write!(&mut self.stdout, "╎")?;

        // Text
        write!(
            &mut self.stdout,
            "{}{}{}{}",
            style::Bold,
            color::Fg(color::Red),
            txt,
            color::Fg(color::Reset),
        )?;

        // Right border
        write!(&mut self.stdout, "╎")?;

        // Bottom border
        write!(&mut self.stdout, "{}", cursor::Goto(mid_x, mid_y + 1))?;
        write!(&mut self.stdout, "╰")?;
        for _ in 0..txt.len() {
            write!(&mut self.stdout, "╴")?;
        }
        write!(&mut self.stdout, "╯")?;

        self.draw_reset()?;

        self.stdout.flush().expect("Flushing game over");

        Ok(())
    }

    /// If apple is in front of the head
    /// then it is considered eaten.
    fn apple_in_front(&self) -> bool {
        let mut head_front = self.snake.head();
        match self.direction {
            Direction::Up => head_front.y -= 1,
            Direction::Down => head_front.y += 1,
            Direction::Left => head_front.x -= 1,
            Direction::Right => head_front.x += 1,
        }
        head_front.collision(&self.apple)
    }

    fn new_apple(&mut self) {
        let mut rng = rand::thread_rng();

        loop {
            let new_row =
                rand::seq::sample_iter(&mut rng, 0..self.field_width - 1, 1).expect("Random row");
            let new_line =
                rand::seq::sample_iter(&mut rng, 0..self.field_height - 1, 1).expect("Random line");

            let new_pos = Point::new(new_row[0], new_line[0]);

            if !new_pos.collision(&self.snake) {
                self.apple.set_position(new_pos);
                break;
            }
        }
    }

    /// Checks whether the snake's head or tail will collide with the wall or itself
    /// in the next step.
    fn collision_next_step(&self) -> bool {
        let head = self.snake.head();
        match self.direction {
            Up => head.y == 0 || Point::new(head.x, head.y - 1).collision(&self.snake),
            Down => {
                head.y == self.field_height - 1
                    || Point::new(head.x, head.y + 1).collision(&self.snake)
            }
            Left => head.x == 0 || Point::new(head.x - 1, head.y).collision(&self.snake),
            Right => {
                head.x == self.field_width - 1
                    || Point::new(head.x + 1, head.y).collision(&self.snake)
            }
        }
    }
}

/// Maps a pressed key to the corresponding direction,
/// if the key is configured.
///
/// |Direction|Keys 1|Keys 2|Keys 3|
/// --------------------------------
/// | Up      | ^    | W    | I    |
/// | Left    | <    | A    | J    |
/// | Down    | v    | S    | K    |
/// | Right   | >    | D    | L    |
fn key_to_direction(key: Key) -> Option<Direction> {
    match key {
        Key::Up | Key::Char('w') | Key::Char('i') => Some(Up),
        Key::Down | Key::Char('s') | Key::Char('k') => Some(Down),
        Key::Left | Key::Char('a') | Key::Char('j') => Some(Left),
        Key::Right | Key::Char('d') | Key::Char('l') => Some(Right),
        _ => None,
    }
}

impl fmt::Display for Game {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for line in 0..self.field_height {
            for row in 0..self.field_width {
                let point = Point::new(row, line);
                if point.collision(&self.apple) {
                    write!(f, "{}", self.apple)?;
                } else if point.collision(&self.snake) {
                    if point == self.snake.head() {
                        write!(f, "ö")?;
                    } else {
                        write!(f, "o")?;
                    }
                } else {
                    write!(f, ".")?;
                }
            }
            write!(f, "\n")?;
        }
        write!(f, "")
    }
}
