use point::Point;
use std::collections::VecDeque;

pub trait Position {
    fn position(&self) -> Point {
        self.positions().front().cloned().expect("Position fron")
    }

    fn positions(&self) -> VecDeque<Point> {
        let mut q = VecDeque::new();
        q.push_back(self.position().clone());
        q
    }

    fn collision(&self, other: &Position) -> bool {
        (self.position() == other.position()) || (self.positions().contains(&other.position()))
            || (other.positions().contains(&self.position()))
    }
}
