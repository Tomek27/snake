//! Terminal Snake Game in ASCII style.
extern crate termion;

mod figure;
mod game;
mod point;
mod traits;

use game::Game;
use std::io::{stdout, Read};
use std::thread::sleep;
use std::time::Duration;
use termion::async_stdin;
use termion::event::{parse_event, Event, Key};
use termion::raw::IntoRawMode;

const FPS: u64 = 10;

fn main() {
    let stdout = stdout().into_raw_mode().unwrap();
    let mut stdin = async_stdin();

    let mut game = Game::new(20, 10, stdout);

    loop {
        game.display().expect("Update display");

        sleep(Duration::from_millis(1_000 / FPS));

        let key = get_async_key(&mut stdin);
        if quit(&key) {
            game.quit().expect("Quitting game");
            return;
        } else if restart(&key) {
            game.restart();
        } else if !game.over() {
            game.input(key);
        }
    }
}

fn get_async_key(stdin: &mut Read) -> Option<Key> {
    let byte = stdin.bytes().next();

    if let Some(byte) = byte {
        if let Ok(byte) = byte {
            let event = parse_event(byte, &mut stdin.bytes()).expect("Parsed Event");
            match event {
                Event::Key(key) => return Some(key),
                _ => return None,
            }
        }
    }

    None
}

fn quit(key: &Option<Key>) -> bool {
    if let Some(key) = *key {
        match key {
            Key::Char('q') | Key::Esc | Key::Ctrl('c') | Key::Ctrl('d') => return true,
            _ => return false,
        }
    }

    false
}

fn restart(key: &Option<Key>) -> bool {
    if let Some(key) = *key {
        match key {
            Key::Char('r') => return true,
            _ => return false,
        }
    }

    false
}
