use point::{Direction, Point};
use std::collections::VecDeque;
use std::default::Default;
use std::fmt;
use termion::color;
use traits::Position;

#[derive(Debug, Clone)]
pub struct Apple {
    position: Point,
}

impl Apple {
    pub fn new(position: Point) -> Self {
        Apple { position }
    }

    pub fn set_position(&mut self, position: Point) {
        self.position = position;
    }
}

impl Position for Apple {
    fn position(&self) -> Point {
        self.position
    }
}

impl fmt::Display for Apple {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{red}@{reset}",
            red = color::Fg(color::Red),
            reset = color::Fg(color::Reset)
        )
    }
}

#[derive(Debug, Clone)]
pub struct Snake {
    positions: VecDeque<Point>,
}

impl Snake {
    pub fn new(position: Point) -> Self {
        Snake::with_length(position, 2)
    }

    pub fn with_length(position: Point, tail_length: usize) -> Self {
        let mut positions = VecDeque::new();
        positions.push_back(position);
        let mut y = position.y;
        for _ in 0..tail_length {
            y += 1;
            positions.push_back(Point::new(position.x, y));
        }

        Snake { positions }
    }

    pub fn crawl(&mut self, direction: &Direction) {
        let mut head = self.positions.front().cloned().expect("Head position");
        match *direction {
            Direction::Up => head.y -= 1,
            Direction::Down => head.y += 1,
            Direction::Left => head.x -= 1,
            Direction::Right => head.x += 1,
        }
        self.positions.push_front(head);
        self.positions.pop_back();
    }

    /// Extends the snake's head by one.
    pub fn grow(&mut self, direction: &Direction) {
        let mut new_head = self.head();
        match *direction {
            Direction::Up => new_head.y -= 1,
            Direction::Down => new_head.y += 1,
            Direction::Left => new_head.x -= 1,
            Direction::Right => new_head.x += 1,
        }
        self.positions.push_front(new_head);
    }

    pub fn head(&self) -> Point {
        self.positions.front().cloned().expect("Snake's head")
    }
}

impl Default for Snake {
    fn default() -> Self {
        Snake::new(Point::default())
    }
}

impl Position for Snake {
    fn positions(&self) -> VecDeque<Point> {
        self.positions.clone()
    }
}

impl fmt::Display for Snake {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "ö")?;
        for _ in 1..self.positions.len() - 1 {
            write!(f, "o")?;
        }
        write!(f, "")
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn position_trait() {
        let apple = Apple::new(Point::new(0, 1));
        let mut points = VecDeque::with_capacity(1);
        points.push_back(Point::new(0, 1));
        assert_eq!(apple.position(), Point::new(0, 1));
        assert_eq!(apple.positions(), points);
    }

    #[test]
    fn collision() {
        let apple = Apple::new(Point::new(0, 0));
        let snake = Snake::new(Point::new(0, 0));
        assert!(apple.collision(&snake));
        assert!(snake.collision(&apple));
    }
}
